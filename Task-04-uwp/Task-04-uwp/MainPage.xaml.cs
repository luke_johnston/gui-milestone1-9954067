﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public MainPage()
        {
            this.InitializeComponent();


        }

        private void btn_search_Click(object sender, RoutedEventArgs e)
        {

            var fruit = new Dictionary<string, string>();

            fruit.Add("apple", "green");
            fruit.Add("mandarin", "orange");
            fruit.Add("peach", "pink");
            fruit.Add("tomatoe", "red");
            fruit.Add("capsicum", "yellow");


            if (fruit.ContainsKey(search_box.Text))
            {
                lbl_result.Text = "Yes the fruit/Vegetable is in the data dictionary!";
            }

            else
            {
                lbl_result.Text = "Sorry that fruit/vegetable is not in the dictionary";
            }

        }
    }
}

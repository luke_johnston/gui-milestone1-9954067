﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btn_calculate_Click(object sender, RoutedEventArgs e)
        {
            var a = num1.Text;
            var b = num2.Text;


            Total.Text = $"{a} + {b} = ${calculate_value(a, b)}";

            Total_gst.Text = $"{a} + {b} = ${total_plus(a, b)}";

        }

        static double calculate_value(string a, string b)
        {
            var a1 = double.Parse(a);
            var b1 = double.Parse(b);
            var caluculated = a1 + b1;
            var gst = caluculated * 1.15;
            var total_add = caluculated + gst;

            return caluculated;
        }

        static double total_plus(string a, string b)
        {
            var a1 = double.Parse(a);
            var b1 = double.Parse(b);
            var caluculated = a1 + b1;
            var gst = caluculated * .115;
            var total_add = caluculated + gst;

            return total_add;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            var q1 = "Application to select out of an array of items";
            var q2 = "Please select from the following options";
            int user_selection;

            Console.WriteLine($"{q1}");
            Console.WriteLine($"{q2}");
            Console.WriteLine("Press 1 for Nissan");
            Console.WriteLine("Press 2 for Toyota");
            Console.WriteLine("Press 3 for Honda");
            Console.WriteLine("Press 4 for Mazda");

            Console.WriteLine("Selection: ");

            user_selection = int.Parse(Console.ReadLine());

            selection(user_selection);

        }

        static void selection (int user_selection) {

            var car_makes = new string[4] { "Nissan", "Toyota", "Honda", "Mazda" };

            if (user_selection == 1)
            {
                Console.WriteLine(car_makes[0]);
            }

            if (user_selection == 2)
            {
                Console.WriteLine(car_makes[1]);
            }

            if (user_selection == 3)
            {
                Console.WriteLine(car_makes[2]);
            }

            if (user_selection == 4)
            {
                Console.WriteLine(car_makes[3]);
            }

            Console.ReadLine();

        }


        }

       
    }


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            var q1 = "Application to select out of an array of items";
            var q2 = "Please select from the following options";

            lbl_header.Text = q1;
            lbl_please_select.Text = q2;

            

        }

        private void combo_cars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cars = new List<string> { "Nissan", "Toyota", "Honda", "Mazda" };

            lbl_selection.Text = $"{cars[combo_cars.SelectedIndex]}";
           
        }
    }
}

﻿namespace Task_01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.input_km = new System.Windows.Forms.TextBox();
            this.km_label = new System.Windows.Forms.Label();
            this.miles_output = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.input_miles = new System.Windows.Forms.TextBox();
            this.lbl_miles = new System.Windows.Forms.Label();
            this.kilo_output = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(293, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Convert Kms to Miles";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(215, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Convert";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // input_km
            // 
            this.input_km.Location = new System.Drawing.Point(202, 76);
            this.input_km.Name = "input_km";
            this.input_km.Size = new System.Drawing.Size(100, 20);
            this.input_km.TabIndex = 2;
            // 
            // km_label
            // 
            this.km_label.AutoSize = true;
            this.km_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.km_label.Location = new System.Drawing.Point(70, 79);
            this.km_label.Name = "km_label";
            this.km_label.Size = new System.Drawing.Size(58, 13);
            this.km_label.TabIndex = 4;
            this.km_label.Text = "Enter Kms ";
            // 
            // miles_output
            // 
            this.miles_output.AutoSize = true;
            this.miles_output.Location = new System.Drawing.Point(341, 82);
            this.miles_output.Name = "miles_output";
            this.miles_output.Size = new System.Drawing.Size(77, 13);
            this.miles_output.TabIndex = 6;
            this.miles_output.Text = "Output in Miles";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(215, 232);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Convert";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // input_miles
            // 
            this.input_miles.Location = new System.Drawing.Point(202, 206);
            this.input_miles.Name = "input_miles";
            this.input_miles.Size = new System.Drawing.Size(100, 20);
            this.input_miles.TabIndex = 8;
            // 
            // lbl_miles
            // 
            this.lbl_miles.AutoSize = true;
            this.lbl_miles.Location = new System.Drawing.Point(70, 209);
            this.lbl_miles.Name = "lbl_miles";
            this.lbl_miles.Size = new System.Drawing.Size(59, 13);
            this.lbl_miles.TabIndex = 9;
            this.lbl_miles.Text = "Enter Miles";
            // 
            // kilo_output
            // 
            this.kilo_output.AutoSize = true;
            this.kilo_output.Location = new System.Drawing.Point(341, 206);
            this.kilo_output.Name = "kilo_output";
            this.kilo_output.Size = new System.Drawing.Size(101, 13);
            this.kilo_output.TabIndex = 10;
            this.kilo_output.Text = "Output in Kilometers";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 381);
            this.Controls.Add(this.kilo_output);
            this.Controls.Add(this.lbl_miles);
            this.Controls.Add(this.input_miles);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.miles_output);
            this.Controls.Add(this.km_label);
            this.Controls.Add(this.input_km);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox input_km;
        private System.Windows.Forms.Label km_label;
        private System.Windows.Forms.Label miles_output;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox input_miles;
        private System.Windows.Forms.Label lbl_miles;
        private System.Windows.Forms.Label kilo_output;
    }
}


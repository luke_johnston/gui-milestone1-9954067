﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var a = input_km.Text;
            miles_output.Text = $"{a} in miles {convertkm(a)}";

        }

        static double convertkm(string input)

        {

            const double km_to_miles = 0.621371192;
            var km = double.Parse(input);
            var calculated = km * km_to_miles;

            return calculated;

        }

    }
}

﻿namespace Task_01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.input_km = new System.Windows.Forms.TextBox();
            this.input_miles = new System.Windows.Forms.TextBox();
            this.km_label = new System.Windows.Forms.Label();
            this.miles_input = new System.Windows.Forms.Label();
            this.miles_output = new System.Windows.Forms.Label();
            this.km_output = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(293, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Convert Kms to Miles";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(204, 206);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Convert";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // input_km
            // 
            this.input_km.Location = new System.Drawing.Point(137, 79);
            this.input_km.Name = "input_km";
            this.input_km.Size = new System.Drawing.Size(100, 20);
            this.input_km.TabIndex = 2;
            // 
            // input_miles
            // 
            this.input_miles.Location = new System.Drawing.Point(137, 137);
            this.input_miles.Name = "input_miles";
            this.input_miles.Size = new System.Drawing.Size(100, 20);
            this.input_miles.TabIndex = 3;
            // 
            // km_label
            // 
            this.km_label.AutoSize = true;
            this.km_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.km_label.Location = new System.Drawing.Point(26, 79);
            this.km_label.Name = "km_label";
            this.km_label.Size = new System.Drawing.Size(58, 13);
            this.km_label.TabIndex = 4;
            this.km_label.Text = "Enter Kms ";
            // 
            // miles_input
            // 
            this.miles_input.AutoSize = true;
            this.miles_input.Location = new System.Drawing.Point(25, 137);
            this.miles_input.Name = "miles_input";
            this.miles_input.Size = new System.Drawing.Size(59, 13);
            this.miles_input.TabIndex = 5;
            this.miles_input.Text = "Enter Miles";
            // 
            // miles_output
            // 
            this.miles_output.AutoSize = true;
            this.miles_output.Location = new System.Drawing.Point(279, 82);
            this.miles_output.Name = "miles_output";
            this.miles_output.Size = new System.Drawing.Size(77, 13);
            this.miles_output.TabIndex = 6;
            this.miles_output.Text = "Output in Miles";
            // 
            // km_output
            // 
            this.km_output.AutoSize = true;
            this.km_output.Location = new System.Drawing.Point(279, 137);
            this.km_output.Name = "km_output";
            this.km_output.Size = new System.Drawing.Size(101, 13);
            this.km_output.TabIndex = 7;
            this.km_output.Text = "Output in Kilometres";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 381);
            this.Controls.Add(this.km_output);
            this.Controls.Add(this.miles_output);
            this.Controls.Add(this.miles_input);
            this.Controls.Add(this.km_label);
            this.Controls.Add(this.input_miles);
            this.Controls.Add(this.input_km);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox input_km;
        private System.Windows.Forms.TextBox input_miles;
        private System.Windows.Forms.Label km_label;
        private System.Windows.Forms.Label miles_input;
        private System.Windows.Forms.Label miles_output;
        private System.Windows.Forms.Label km_output;
    }
}


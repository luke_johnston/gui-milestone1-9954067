﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var q1 = "Console Application to Search Data Dictionary";
            var q2 = "Search Vegetable or Fruit from database";

            lbl_header.Text = q1;
            lbl_search.Text = q2;

        }

        public void btn_search_Click(object sender, EventArgs e)
        {

            var fruit = new Dictionary<string, string>();

            fruit.Add("apple", "green");
            fruit.Add("mandarin", "orange");
            fruit.Add("peach", "pink");
            fruit.Add("tomatoe", "red");
            fruit.Add("capsicum", "yellow");

      

            if (fruit.ContainsKey(search_box.Text))
            {
                lbl_result.Text = "Yes the fruit/Vegetable is in the data dictionary!";
            }

            else
            {
                lbl_result.Text = "Sorry that fruit/vegetable is not in the dictionary";
            }
        
            }
        }
}

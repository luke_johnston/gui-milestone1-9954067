﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {

            int km_mile_selector;
            var input_from_user = "";
            double km_value = 0;
            double miles_value = 0;


            Console.WriteLine("This Program is to convert Kilometres Km to Miles MPH (VISE VERSA)");

            Console.WriteLine("Do you wish to convert from kms to miles (Press 1) or miles to kms (Press 2)");

            km_mile_selector = int.Parse(Console.ReadLine());



            if (km_mile_selector == 1)
            {
                Console.WriteLine("Please enter the number of kms you wish to convert");
                input_from_user = Console.ReadLine();
                km_value = convertkm(input_from_user);
                Console.WriteLine($"{input_from_user} in Miles is {km_value}");
                Console.ReadKey();


            }

            else
            {
                Console.WriteLine("Please enter the number of miles you wish to convert");
                input_from_user = Console.ReadLine();
                miles_value = convertmiles(input_from_user);
                Console.WriteLine($"{input_from_user} in Kilometers is {miles_value}");
                Console.ReadKey();
            }
      
            
        }

        static double convertkm(string input)

        {

            const double km_to_miles = 0.621371192;
            var km = double.Parse(input);
            var calculated = km * km_to_miles;

            return calculated;

        }


        static double convertmiles(string input)

        {

            const double miles_to_km = 1.60934;
            var miles = double.Parse(input);
            var calculated = miles * miles_to_km;

            return calculated;

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {

            var q1 = "This is a game, guess a number between 1 and 5 to earn points!";
            var q2 = "You have 5 tries!";
            var q3 = "What is your guess: ";


            int user_guess;
            int[] numbers = new int[5] { 1, 2, 3, 4, 5 };
            Random rand = new Random();
            int answer = numbers[rand.Next(0, numbers.Length)];


            Console.WriteLine($"{q1}");
            Console.WriteLine($"{q2}");

            Console.WriteLine($"{q3}");

            user_guess = int.Parse(Console.ReadLine());


            var counter = 5;
            var i = 0;
            var score = 0;
            
            
            for (i = 0; i < counter;) {


                Console.WriteLine("Your guess was: " + user_guess);
                Console.WriteLine("The answer was: " + answer);


                if (user_guess == answer)
                {

                    score = score + 1;
                    Console.WriteLine("That was a correct guess, you scored a point!");
                    Console.WriteLine("Guesses used:" + i);
                    Console.WriteLine("Your score: " + score);

                }

                else
                {

                    i = i + 1;
                    Console.WriteLine("incorrect!");
                    Console.WriteLine("Guesses used:" + i);
                }

                Console.ReadKey();

                            

            }
            Console.WriteLine("Thanks for playing your end score was: " + score);
            Console.ReadKey();
    }
        
    }
    }
    
    

        
    


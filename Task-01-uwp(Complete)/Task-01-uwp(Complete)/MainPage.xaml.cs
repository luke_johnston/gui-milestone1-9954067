﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp_Complete_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var a = input_km.Text;
            miles_output.Text = $"{a} in miles {convertkm(a)}";
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var b = input_miles.Text;
            kilo_output.Text = $"{b} in miles {convertmiles(b)}";
        }

        static double convertkm(string input)

        {

            const double km_to_miles = 0.621371192;
            var km = double.Parse(input);
            var calculated = km * km_to_miles;

            return calculated;

        }

        static double convertmiles(string input)

        {

            const double miles_to_km = 1.60934;
            var miles = double.Parse(input);
            var calculated = miles * miles_to_km;

            return calculated;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var q1 = "Application to select out of an array of items";
            var q2 = "Please select from the following options";

            lbl_header.Text = q1;
            lbl_please_select.Text = q2;


            var cars = new List<string> { "Nissan", "Toyota", "Honda", "Mazda" };
            combo_cars.DataSource = cars;

        }

        private void btn_select_Click(object sender, EventArgs e)
        {
            lbl_selection.Text = "The car you selected was: " + combo_cars.Text;

        }


    }
}

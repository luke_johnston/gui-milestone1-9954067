﻿namespace Task_03_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_select = new System.Windows.Forms.Button();
            this.lbl_selection = new System.Windows.Forms.Label();
            this.combo_cars = new System.Windows.Forms.ComboBox();
            this.lbl_header = new System.Windows.Forms.Label();
            this.lbl_please_select = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_select
            // 
            this.btn_select.Location = new System.Drawing.Point(138, 147);
            this.btn_select.Name = "btn_select";
            this.btn_select.Size = new System.Drawing.Size(75, 23);
            this.btn_select.TabIndex = 0;
            this.btn_select.Text = "Select";
            this.btn_select.UseVisualStyleBackColor = true;
            this.btn_select.Click += new System.EventHandler(this.btn_select_Click);
            // 
            // lbl_selection
            // 
            this.lbl_selection.AutoSize = true;
            this.lbl_selection.Location = new System.Drawing.Point(151, 213);
            this.lbl_selection.Name = "lbl_selection";
            this.lbl_selection.Size = new System.Drawing.Size(51, 13);
            this.lbl_selection.TabIndex = 1;
            this.lbl_selection.Text = "Selection";
            // 
            // combo_cars
            // 
            this.combo_cars.FormattingEnabled = true;
            this.combo_cars.Location = new System.Drawing.Point(118, 109);
            this.combo_cars.Name = "combo_cars";
            this.combo_cars.Size = new System.Drawing.Size(121, 21);
            this.combo_cars.TabIndex = 2;
            // 
            // lbl_header
            // 
            this.lbl_header.AutoSize = true;
            this.lbl_header.Location = new System.Drawing.Point(90, 20);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(0, 13);
            this.lbl_header.TabIndex = 3;
            // 
            // lbl_please_select
            // 
            this.lbl_please_select.AutoSize = true;
            this.lbl_please_select.Location = new System.Drawing.Point(115, 79);
            this.lbl_please_select.Name = "lbl_please_select";
            this.lbl_please_select.Size = new System.Drawing.Size(0, 13);
            this.lbl_please_select.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 406);
            this.Controls.Add(this.lbl_please_select);
            this.Controls.Add(this.lbl_header);
            this.Controls.Add(this.combo_cars);
            this.Controls.Add(this.lbl_selection);
            this.Controls.Add(this.btn_select);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_select;
        private System.Windows.Forms.Label lbl_selection;
        private System.Windows.Forms.ComboBox combo_cars;
        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.Label lbl_please_select;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf_ver2_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var q1 = "This is an application to calculate user input + gst";
            var q2 = "Please enter the first value in $:";
            var q3 = "Please enter the second value in $:";
            

            lbl_header.Text = q1;
            lbl_first.Text = q2;
            lbl_second.Text = q3;



        }

        private void btn_calc_Click(object sender, EventArgs e)
        {


            lbl_total.Text = $"{calculation(first_input.Text, second_input.Text)}";

            lbl_gst.Text = $"{gst(calculation(first_input.Text, second_input.Text))}";
        }

        static double calculation(string num1, string num2)
        {
            double num1a = double.Parse(num1);
            double num2a = double.Parse(num2);

            double total;
            total = num1a + num2a;
            return total;
        }

        static double gst(double total)
        {
            double add;
            add = total * 1.15;
            return add;
        }
    }
}

﻿namespace Task_02_wf_ver2_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.first_input = new System.Windows.Forms.TextBox();
            this.second_input = new System.Windows.Forms.TextBox();
            this.lbl_first = new System.Windows.Forms.Label();
            this.lbl_second = new System.Windows.Forms.Label();
            this.lbl_header = new System.Windows.Forms.Label();
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_total = new System.Windows.Forms.Label();
            this.lbl_gst = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // first_input
            // 
            this.first_input.Location = new System.Drawing.Point(273, 106);
            this.first_input.Name = "first_input";
            this.first_input.Size = new System.Drawing.Size(133, 20);
            this.first_input.TabIndex = 0;
            // 
            // second_input
            // 
            this.second_input.Location = new System.Drawing.Point(273, 147);
            this.second_input.Name = "second_input";
            this.second_input.Size = new System.Drawing.Size(133, 20);
            this.second_input.TabIndex = 1;
            // 
            // lbl_first
            // 
            this.lbl_first.AutoSize = true;
            this.lbl_first.Location = new System.Drawing.Point(12, 113);
            this.lbl_first.Name = "lbl_first";
            this.lbl_first.Size = new System.Drawing.Size(35, 13);
            this.lbl_first.TabIndex = 2;
            this.lbl_first.Text = "label1";
            // 
            // lbl_second
            // 
            this.lbl_second.AutoSize = true;
            this.lbl_second.Location = new System.Drawing.Point(12, 147);
            this.lbl_second.Name = "lbl_second";
            this.lbl_second.Size = new System.Drawing.Size(35, 13);
            this.lbl_second.TabIndex = 3;
            this.lbl_second.Text = "label2";
            // 
            // lbl_header
            // 
            this.lbl_header.AutoSize = true;
            this.lbl_header.Location = new System.Drawing.Point(188, 27);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(35, 13);
            this.lbl_header.TabIndex = 4;
            this.lbl_header.Text = "label3";
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(180, 202);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 5;
            this.btn_calc.Text = "calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Location = new System.Drawing.Point(199, 265);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(31, 13);
            this.lbl_total.TabIndex = 6;
            this.lbl_total.Text = "Total";
            // 
            // lbl_gst
            // 
            this.lbl_gst.AutoSize = true;
            this.lbl_gst.Location = new System.Drawing.Point(199, 304);
            this.lbl_gst.Name = "lbl_gst";
            this.lbl_gst.Size = new System.Drawing.Size(54, 13);
            this.lbl_gst.TabIndex = 7;
            this.lbl_gst.Text = "Total +gst";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 464);
            this.Controls.Add(this.lbl_gst);
            this.Controls.Add(this.lbl_total);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.lbl_header);
            this.Controls.Add(this.lbl_second);
            this.Controls.Add(this.lbl_first);
            this.Controls.Add(this.second_input);
            this.Controls.Add(this.first_input);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox first_input;
        private System.Windows.Forms.TextBox second_input;
        private System.Windows.Forms.Label lbl_first;
        private System.Windows.Forms.Label lbl_second;
        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label lbl_gst;
    }
}


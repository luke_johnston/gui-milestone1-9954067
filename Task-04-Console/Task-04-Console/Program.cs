﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            var q1 = "Console Application to Search Data Dictionary";
            var q2 = "Search Vegetable or Fruit from database";
            var q3 = "Search here:";


            var search = "";

            Console.WriteLine($"{q1}");
            Console.WriteLine($"{q2}");
            Console.WriteLine($"{q3}");

            search = Console.ReadLine();

            search_return(search);

        }


        static void search_return(string search) {

            var fruit = new Dictionary<string, string>();

            fruit.Add("apple", "green");
            fruit.Add("mandarin", "orange");
            fruit.Add("peach", "pink");
            fruit.Add("tomatoe", "red");
            fruit.Add("capsicum", "yellow");


            if (fruit.ContainsKey($"{search}"))
            {

                Console.WriteLine("Yes the fruit/Vegetable is in the data dictionary!");

                Console.ReadKey();

            }

            else
            {
                Console.WriteLine("Sorry this Fruit/Vegetable does not exist in the data dictionary");

                Console.ReadKey();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var q1 = "This is a game, guess a number between 1 and 5 to earn points!";
            var q2 = "You have 5 tries!";
            var q3 = "What is your guess: ";

            lbl_header.Text = q1;
            lbl_sub.Text = q2;
            lbl_question.Text = q3;

        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            int guess;
            int[] numbers = new int[5] { 1, 2, 3, 4, 5 };
            Random rand = new Random();
            int answer = numbers[rand.Next(0, numbers.Length)];
            int i = 0;
            int counter = 5;
            var score = 0;




            guess = int.Parse(user_guess.Text);

            for (i = 0; i < counter; i++)
            {


                if (guess == answer)
                {
                    score = score + 1;

                    lbl_answer.Text = "Correct! You Scored a Point!";
                    lbl_tries.Text = "Turns used: " +i;
                    lbl_score.Text = "Your score is: " + score;
                }

                else
                {
                    
                    lbl_answer.Text = "Incorrect try again!";
                    lbl_tries.Text = "Turns used: " + i;
                    lbl_score.Text = "Your score is: " + score;
                }


            }
        }
    }
}

﻿namespace Task_05_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_header = new System.Windows.Forms.Label();
            this.lbl_sub = new System.Windows.Forms.Label();
            this.lbl_question = new System.Windows.Forms.Label();
            this.user_guess = new System.Windows.Forms.TextBox();
            this.btn_play = new System.Windows.Forms.Button();
            this.lbl_answer = new System.Windows.Forms.Label();
            this.lbl_tries = new System.Windows.Forms.Label();
            this.lbl_score = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_header
            // 
            this.lbl_header.AutoSize = true;
            this.lbl_header.Location = new System.Drawing.Point(66, 9);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(10, 13);
            this.lbl_header.TabIndex = 0;
            this.lbl_header.Text = ".";
            // 
            // lbl_sub
            // 
            this.lbl_sub.AutoSize = true;
            this.lbl_sub.Location = new System.Drawing.Point(66, 45);
            this.lbl_sub.Name = "lbl_sub";
            this.lbl_sub.Size = new System.Drawing.Size(10, 13);
            this.lbl_sub.TabIndex = 1;
            this.lbl_sub.Text = ".";
            // 
            // lbl_question
            // 
            this.lbl_question.AutoSize = true;
            this.lbl_question.Location = new System.Drawing.Point(66, 79);
            this.lbl_question.Name = "lbl_question";
            this.lbl_question.Size = new System.Drawing.Size(10, 13);
            this.lbl_question.TabIndex = 2;
            this.lbl_question.Text = ".";
            // 
            // user_guess
            // 
            this.user_guess.Location = new System.Drawing.Point(29, 110);
            this.user_guess.Name = "user_guess";
            this.user_guess.Size = new System.Drawing.Size(361, 20);
            this.user_guess.TabIndex = 3;
            // 
            // btn_play
            // 
            this.btn_play.Location = new System.Drawing.Point(29, 136);
            this.btn_play.Name = "btn_play";
            this.btn_play.Size = new System.Drawing.Size(361, 23);
            this.btn_play.TabIndex = 4;
            this.btn_play.Text = "Play";
            this.btn_play.UseVisualStyleBackColor = true;
            this.btn_play.Click += new System.EventHandler(this.btn_play_Click);
            // 
            // lbl_answer
            // 
            this.lbl_answer.AutoSize = true;
            this.lbl_answer.Location = new System.Drawing.Point(160, 175);
            this.lbl_answer.Name = "lbl_answer";
            this.lbl_answer.Size = new System.Drawing.Size(42, 13);
            this.lbl_answer.TabIndex = 5;
            this.lbl_answer.Text = "Answer";
            // 
            // lbl_tries
            // 
            this.lbl_tries.AutoSize = true;
            this.lbl_tries.Location = new System.Drawing.Point(159, 211);
            this.lbl_tries.Name = "lbl_tries";
            this.lbl_tries.Size = new System.Drawing.Size(87, 13);
            this.lbl_tries.TabIndex = 6;
            this.lbl_tries.Text = "Turns Remaining";
            // 
            // lbl_score
            // 
            this.lbl_score.AutoSize = true;
            this.lbl_score.Location = new System.Drawing.Point(159, 248);
            this.lbl_score.Name = "lbl_score";
            this.lbl_score.Size = new System.Drawing.Size(35, 13);
            this.lbl_score.TabIndex = 7;
            this.lbl_score.Text = "Score";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 445);
            this.Controls.Add(this.lbl_score);
            this.Controls.Add(this.lbl_tries);
            this.Controls.Add(this.lbl_answer);
            this.Controls.Add(this.btn_play);
            this.Controls.Add(this.user_guess);
            this.Controls.Add(this.lbl_question);
            this.Controls.Add(this.lbl_sub);
            this.Controls.Add(this.lbl_header);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.Label lbl_sub;
        private System.Windows.Forms.Label lbl_question;
        private System.Windows.Forms.TextBox user_guess;
        private System.Windows.Forms.Button btn_play;
        private System.Windows.Forms.Label lbl_answer;
        private System.Windows.Forms.Label lbl_tries;
        private System.Windows.Forms.Label lbl_score;
    }
}

